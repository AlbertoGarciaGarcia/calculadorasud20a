package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JComboBox;

public class Principal extends JFrame {

	private JPanel contentPane;
	
	private static JLabel lblIcon1;
	
	private static JLabel lblIcon2;
	
	private JComboBox comboDivisas1;
	
	private JComboBox comboDivisas2;
	
	private static JTextArea divisa1;
	
	private JTextArea divisa2;
	
	private JButton btn0;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;
	private JButton btn7;
	private JButton btn8;
	private JButton btn9;
	private JButton btnComa;

	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 782, 564);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnCE = new JButton("CE");
		btnCE.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				divisa1.setText(null); 
				divisa2.setText(null);
			}
		});
		btnCE.setBounds(514, 51, 113, 82);
		contentPane.add(btnCE);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				divisa1.setText(""+divisa1.getText().substring(0, divisa1.getText().length() - 1));
			}
		});
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnBorrar.setBounds(639, 51, 113, 82);
		contentPane.add(btnBorrar);
		
		 btn8 = new JButton("8");
		btn8.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn8.setBounds(514, 146, 113, 82);
		contentPane.add(btn8);
		btn8.addActionListener(escribir);
		
		 btn9 = new JButton("9");
		btn9.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn9.setBounds(639, 146, 113, 82);
		contentPane.add(btn9);
		btn9.addActionListener(escribir);
		
		 btn7 = new JButton("7");
		btn7.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn7.setBounds(389, 146, 113, 82);
		contentPane.add(btn7);
		btn7.addActionListener(escribir);
		
		 btn5 = new JButton("5");
		btn5.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn5.setBounds(514, 241, 113, 82);
		contentPane.add(btn5);
		btn5.addActionListener(escribir);
		
		 btn6 = new JButton("6");
		btn6.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn6.setBounds(639, 241, 113, 82);
		contentPane.add(btn6);
		btn6.addActionListener(escribir);
		
		 btn4 = new JButton("4");
		btn4.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn4.setBounds(389, 241, 113, 82);
		contentPane.add(btn4);
		btn4.addActionListener(escribir);
		
		 btn2 = new JButton("2");
		btn2.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn2.setBounds(514, 336, 113, 82);
		contentPane.add(btn2);
		btn2.addActionListener(escribir);
		
		 btn3 = new JButton("3");
		btn3.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn3.setBounds(639, 336, 113, 82);
		contentPane.add(btn3);
		btn3.addActionListener(escribir);
		
		 btn1 = new JButton("1");
		btn1.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn1.setBounds(389, 336, 113, 82);
		contentPane.add(btn1);
		btn1.addActionListener(escribir);
		
		 btn0 = new JButton("0");
		btn0.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn0.setBounds(514, 431, 113, 82);
		contentPane.add(btn0);
		btn0.addActionListener(escribir);
		
		 btnComa = new JButton(",");
		btnComa.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnComa.setBounds(639, 431, 113, 82);
		contentPane.add(btnComa);
		btnComa.addActionListener(escribir);
		
		 divisa1 = new JTextArea();
		 divisa1.setFont(new Font("Monospaced", Font.PLAIN, 18));
		divisa1.setBounds(34, 99, 177, 46);
		contentPane.add(divisa1);
		
		 divisa2 = new JTextArea();
		 divisa2.setFont(new Font("Monospaced", Font.PLAIN, 18));
		divisa2.setBounds(34, 272, 177, 46);
		contentPane.add(divisa2);
		
		 lblIcon1 = new JLabel("€");
		lblIcon1.setFont(new Font("Tahoma", Font.PLAIN, 32));
		lblIcon1.setBounds(223, 99, 113, 46);
		contentPane.add(lblIcon1);
		
		 lblIcon2 = new JLabel("€");
		lblIcon2.setFont(new Font("Tahoma", Font.PLAIN, 32));
		lblIcon2.setBounds(223, 272, 113, 46);
		contentPane.add(lblIcon2);
		
		 comboDivisas1 = new JComboBox();
		comboDivisas1.setBounds(34, 158, 217, 22);
		contentPane.add(comboDivisas1);
		rellenarComboBox(comboDivisas1);
		comboDivisas1.addActionListener(cambiarIcono);
		
		 comboDivisas2 = new JComboBox();
		comboDivisas2.setBounds(34, 336, 217, 22);
		contentPane.add(comboDivisas2);
		rellenarComboBox(comboDivisas2);
		comboDivisas2.addActionListener(cambiarIcono);
		
		
	}
	
	public void rellenarComboBox (JComboBox comboDivisas) {
		
		comboDivisas.addItem("Europa - Euros");
		comboDivisas.addItem("Estados Unidos - Dólar");
		comboDivisas.addItem("Reino Unido - Libra");
		comboDivisas.addItem("China - Yuan");
		comboDivisas.addItem("Emiratos Árabes Unidos - Dírham");
		comboDivisas.addItem("Argentina - Peso Argentino");
	}
	
	public static String getTexto() {
		String texto = "";
		texto = divisa1.getText();
		return texto;
	}
	
	public static double conversionDivisa() {
		
		double dinero = 0;
		
		dinero = Double.parseDouble(divisa1.getText());
		
		// EUROS---------------------
		if (lblIcon1.getText().equalsIgnoreCase("€") && lblIcon2.getText().equalsIgnoreCase("$")) {
			dinero = dinero * 1.19;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("€") && lblIcon2.getText().equalsIgnoreCase("£")) {
			dinero = dinero * 0.90;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("€") && lblIcon2.getText().equalsIgnoreCase("¥")) {
			dinero = dinero * 8.20;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("€") && lblIcon2.getText().equalsIgnoreCase("د.إ")) {
			dinero = dinero * 4.36;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("€") && lblIcon2.getText().equalsIgnoreCase("$Arg")) {
			dinero = dinero * 87.28;
			return dinero;
		}
		
		// DOLARES------------------------------
		if (lblIcon1.getText().equalsIgnoreCase("$") && lblIcon2.getText().equalsIgnoreCase("€")) {
			dinero = dinero * 0.84;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("$") && lblIcon2.getText().equalsIgnoreCase("£")) {
			dinero = dinero * 0.76;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("$") && lblIcon2.getText().equalsIgnoreCase("¥")) {
			dinero = dinero * 6.92;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("$") && lblIcon2.getText().equalsIgnoreCase("د.إ")) {
			dinero = dinero * 3.67;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("$") && lblIcon2.getText().equalsIgnoreCase("$Arg")) {
			dinero = dinero * 73.47;
			return dinero;
		}
		
		// LIBRAS------------------------------
		if (lblIcon1.getText().equalsIgnoreCase("£") && lblIcon2.getText().equalsIgnoreCase("$")) {
			dinero = dinero * 1.32;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("£") && lblIcon2.getText().equalsIgnoreCase("€")) {
			dinero = dinero * 1.11;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("£") && lblIcon2.getText().equalsIgnoreCase("¥")) {
			dinero = dinero * 9.12;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("£") && lblIcon2.getText().equalsIgnoreCase("د.إ")) {
			dinero = dinero * 4.83;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("£") && lblIcon2.getText().equalsIgnoreCase("$Arg")) {
			dinero = dinero * 96.79;
			return dinero;
		}
		
		// YUANES-----------------------------
		if (lblIcon1.getText().equalsIgnoreCase("¥") && lblIcon2.getText().equalsIgnoreCase("$")) {
			dinero = dinero * 0.14;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("¥") && lblIcon2.getText().equalsIgnoreCase("£")) {
			dinero = dinero * 0.11;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("¥") && lblIcon2.getText().equalsIgnoreCase("€")) {
			dinero = dinero * 0.12;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("¥") && lblIcon2.getText().equalsIgnoreCase("د.إ")) {
			dinero = dinero * 0.53;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("¥") && lblIcon2.getText().equalsIgnoreCase("$Arg")) {
			dinero = dinero * 10.62;
			return dinero;
		}
		
		// DIRHAMS EMIRATOS ARABES-------------------------
		if (lblIcon1.getText().equalsIgnoreCase("د.إ") && lblIcon2.getText().equalsIgnoreCase("$")) {
			dinero = dinero * 0.27;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("د.إ") && lblIcon2.getText().equalsIgnoreCase("£")) {
			dinero = dinero * 0.21;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("د.إ") && lblIcon2.getText().equalsIgnoreCase("¥")) {
			dinero = dinero * 1.88;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("د.إ") && lblIcon2.getText().equalsIgnoreCase("€")) {
			dinero = dinero * 0.23;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("د.إ") && lblIcon2.getText().equalsIgnoreCase("$Arg")) {
			dinero = dinero * 20.00;
			return dinero;
		}
		
		
		// PESOS ARGENTINOS-------------------------------
		if (lblIcon1.getText().equalsIgnoreCase("$Arg") && lblIcon2.getText().equalsIgnoreCase("$")) {
			dinero = dinero * 0.014;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("$Arg") && lblIcon2.getText().equalsIgnoreCase("£")) {
			dinero = dinero * 0.010;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("$Arg") && lblIcon2.getText().equalsIgnoreCase("¥")) {
			dinero = dinero * 0.094;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("$Arg") && lblIcon2.getText().equalsIgnoreCase("د.إ")) {
			dinero = dinero * 0.050;
			return dinero;
		}
		else if (lblIcon1.getText().equalsIgnoreCase("$Arg") && lblIcon2.getText().equalsIgnoreCase("€")) {
			dinero = dinero * 0.011;
			return dinero;
		}
		
		return dinero;
	}
	
	ActionListener cambiarIcono = new ActionListener() {

		public void actionPerformed(ActionEvent e) {
			
			String divisaEscogida1 = "";
			
			String divisaEscogida2 = "";
			
			divisaEscogida1 = comboDivisas1.getSelectedItem().toString();
			
			divisaEscogida2 = comboDivisas2.getSelectedItem().toString();
			
			if (divisaEscogida1.equalsIgnoreCase("Europa - Euros")) {
				lblIcon1.setText("€");
				
			}
			else if (divisaEscogida1.equalsIgnoreCase("Estados Unidos - Dólar")) {
				lblIcon1.setText("$");
				
			}
			else if (divisaEscogida1.equalsIgnoreCase("Reino Unido - Libra")) {
				lblIcon1.setText("£");
				
			}
			else if (divisaEscogida1.equalsIgnoreCase("China - Yuan")) {
				lblIcon1.setText("¥");
				
			}
			else if (divisaEscogida1.equalsIgnoreCase("Emiratos Árabes Unidos - Dírham")) {
				lblIcon1.setText("د.إ");
				
			}
			else {
				lblIcon1.setText("$Arg");
				
			}
			
			if (divisaEscogida2.equalsIgnoreCase("Europa - Euros")) {
				lblIcon2.setText("€");
				
			}
			else if (divisaEscogida2.equalsIgnoreCase("Estados Unidos - Dólar")) {
				lblIcon2.setText("$");
				
			}
			else if (divisaEscogida2.equalsIgnoreCase("Reino Unido - Libra")) {
				lblIcon2.setText("£");
				
			}
			else if (divisaEscogida2.equalsIgnoreCase("China - Yuan")) {
				lblIcon2.setText("¥");
				
			}
			else if (divisaEscogida2.equalsIgnoreCase("Emiratos Árabes Unidos - Dírham")) {
				lblIcon2.setText("د.إ");
				
			}
			else {
				lblIcon2.setText("$Arg");
				
			}
		}
	};
	
	ActionListener escribir = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			double conversion = 0;
			
			if (e.getSource() == btn1) {
				divisa1.setText(getTexto() + "1"); 
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btn2) {
				divisa1.setText(getTexto() + "2");
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btn3) {
				divisa1.setText(getTexto() + "3");
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btn4) {
				divisa1.setText(getTexto() + "4");
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btn5) {
				divisa1.setText(getTexto() + "5");
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btn6) {
				divisa1.setText(getTexto() + "6");
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btn7) {
				divisa1.setText(getTexto() + "7");
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btn8) {
				divisa1.setText(getTexto() + "8");
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btn9) {
				divisa1.setText(getTexto() + "9");
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btn0) {
				divisa1.setText(getTexto() + "0");
				conversion = conversionDivisa();
				divisa2.setText(String.valueOf(conversion));
			}
			else if (e.getSource() == btnComa) {
				divisa1.setText(getTexto() + ".");
			}
			
		}
	};
}
