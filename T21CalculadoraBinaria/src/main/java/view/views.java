package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class views extends JFrame {
	
	
	private static JLabel lblResultado = new JLabel("");
	private static JLabel lblResultadoAux = new JLabel("");
	private static JLabel lblHex, lblDec, lblOct, lblBin;
	private static int num1;
	private static int num2;
	private static int resultado;
	private static String operador;
	
	public views() {
		getContentPane().setLayout(null);
		
		//-----BOTONES-----
		JButton btn7 = new JButton("7");
		btn7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 7);
				transformarNumero();
			}
		});
		btn7.setBounds(25, 214, 60, 50);
		getContentPane().add(btn7);
		
		
		JButton btn8 = new JButton("8");
		btn8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 8);
				transformarNumero();
			}
		});
		btn8.setBounds(111, 214, 60, 50);
		getContentPane().add(btn8);
		
		
		JButton btn9 = new JButton("9");
		btn9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 9);
				transformarNumero();
			}
		});
		btn9.setBounds(194, 214, 60, 50);
		getContentPane().add(btn9);
		
		
		JButton btn4 = new JButton("4");
		btn4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 4);
				transformarNumero();
			}
		});
		btn4.setBounds(25, 275, 60, 50);
		getContentPane().add(btn4);
		
		
		JButton btn5 = new JButton("5");
		btn5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 5);
				transformarNumero();
			}
		});
		btn5.setBounds(111, 275, 60, 50);
		getContentPane().add(btn5);
		
		
		JButton btn6 = new JButton("6");
		btn6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 6);
				transformarNumero();
			}
		});
		btn6.setBounds(194, 275, 60, 50);
		getContentPane().add(btn6);
		
		
		JButton btn1 = new JButton("1");
		btn1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 1);
				transformarNumero();
			}
		});
		btn1.setBounds(25, 336, 60, 50);
		getContentPane().add(btn1);
		
		
		JButton btn2 = new JButton("2");
		btn2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 2);
				transformarNumero();
			}
		});
		btn2.setBounds(111, 336, 60, 50);
		getContentPane().add(btn2);
		
		
		JButton btn3 = new JButton("3");
		btn3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 3);
				transformarNumero();
			}
		});
		btn3.setBounds(194, 336, 60, 50);
		getContentPane().add(btn3);
		
		
		JButton btn0 = new JButton("0");
		btn0.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 0);
				transformarNumero();
			}
		});
		btn0.setBounds(25, 397, 146, 50);
		getContentPane().add(btn0);
		lblResultado.setHorizontalAlignment(SwingConstants.RIGHT);
		
		//-----Label de resultado-----
		lblResultado.setBounds(149, 83, 175, 42);
		getContentPane().add(lblResultado);
		setTitle("Calculadora Programador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 644, 497);
		setVisible(true);
		
		final JTextArea textHistorial = new JTextArea();
		textHistorial.setBounds(350, 54, 254, 393);
		getContentPane().add(textHistorial);

		
		JButton btnMultiplicacion = new JButton("X");
		btnMultiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vaciarLbl();
				operador = "x";
				num2 =  Integer.parseInt(lblResultado.getText());
				lblResultadoAux.setText(lblResultado.getText() + "x");
				lblResultado.setText("");

			}
		});
		btnMultiplicacion.setBounds(280, 275, 60, 50);
		getContentPane().add(btnMultiplicacion);
		
		JButton btnResta = new JButton("-");
		btnResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vaciarLbl();
				operador = "-";
				num2 = Integer.parseInt(lblResultado.getText());
				lblResultadoAux.setText(lblResultado.getText() + "-");
				lblResultado.setText("");
			}
		});
		btnResta.setBounds(281, 336, 59, 50);
		getContentPane().add(btnResta);
		
		JButton btnSuma = new JButton("+");
		btnSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vaciarLbl();
				operador = "+";
				num2 =  Integer.parseInt(lblResultado.getText());
				lblResultadoAux.setText(lblResultado.getText() + "+");
				lblResultado.setText("");

			}
		});
		btnSuma.setBounds(280, 397, 60, 50);
		getContentPane().add(btnSuma);
		
		JButton btnDivision = new JButton("/");
		btnDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vaciarLbl();
				operador = "/";
				num2 =  Integer.parseInt(lblResultado.getText());
				lblResultadoAux.setText(lblResultado.getText() + "/");
				lblResultado.setText("");

			}
		});
		btnDivision.setBounds(280, 214, 60, 50);
		getContentPane().add(btnDivision);
		
		JButton btnCE = new JButton("CE");
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblResultado.setText("");
				num1 = 0;
				vaciarLbl();

			}
		});
		btnCE.setBounds(25, 153, 60, 50);
		getContentPane().add(btnCE);
		
		JButton btnC = new JButton("C");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblResultado.setText("");
				lblResultadoAux.setText("");
				num1 = 0;
				num2 = 0;
				vaciarLbl();
			}
		});
		btnC.setBounds(111, 153, 60, 50);
		getContentPane().add(btnC);
		
		JButton btnDelete = new JButton("Del");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblResultado.setText(""+lblResultado.getText().substring(0, lblResultado.getText().length() - 1));
			}
		});
		btnDelete.setBounds(194, 153, 146, 50);
		getContentPane().add(btnDelete);
		

		JButton btnIgual = new JButton("=");
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vaciarLbl();
				num1 = Integer.parseInt(lblResultado.getText());
				if (operador.contentEquals("+")) {
					resultado = num2 + num1;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "+" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("-")) {
					resultado = num2 - num1;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "-" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("/")) {
					resultado = num2 / num1;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "/" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("x")) {
					resultado = num2 * num1;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "x" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("%")) {
					resultado = (num1/100) * num2;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "%" + num1 + "=" + resultado + "\n"));
				}
			}
		});
		btnIgual.setBounds(194, 397, 60, 50);
		getContentPane().add(btnIgual);
		
		//-----Label de historial-----
		JLabel lblEnunciado = new JLabel("Resultado");
		lblEnunciado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEnunciado.setBounds(25, 26, 87, 19);
		getContentPane().add(lblEnunciado);
		
		JLabel lblHistorial = new JLabel("Historial");
		lblHistorial.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblHistorial.setBounds(350, 26, 87, 19);
		getContentPane().add(lblHistorial);
		
		lblResultadoAux = new JLabel("");
		lblResultadoAux.setHorizontalAlignment(SwingConstants.RIGHT);
		lblResultadoAux.setBounds(149, 56, 175, 19);
		getContentPane().add(lblResultadoAux);
		
		JLabel lblTextoHex = new JLabel("HEX");
		lblTextoHex.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTextoHex.setBounds(10, 58, 31, 19);
		getContentPane().add(lblTextoHex);
		
		JLabel lblTextoDec = new JLabel("DEC");
		lblTextoDec.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTextoDec.setBounds(10, 83, 31, 19);
		getContentPane().add(lblTextoDec);
		
		JLabel lblTextoOct = new JLabel("OCT");
		lblTextoOct.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTextoOct.setBounds(10, 106, 31, 19);
		getContentPane().add(lblTextoOct);
		
		JLabel lblTextoBin = new JLabel("BIN");
		lblTextoBin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTextoBin.setBounds(10, 131, 31, 19);
		getContentPane().add(lblTextoBin);
		
		lblHex = new JLabel("");
		lblHex.setHorizontalAlignment(SwingConstants.LEFT);
		lblHex.setBounds(35, 58, 105, 19);
		getContentPane().add(lblHex);
		
		lblDec = new JLabel("");
		lblDec.setHorizontalAlignment(SwingConstants.LEFT);
		lblDec.setBounds(34, 83, 105, 19);
		getContentPane().add(lblDec);
		
		lblOct = new JLabel("");
		lblOct.setHorizontalAlignment(SwingConstants.LEFT);
		lblOct.setBounds(34, 106, 105, 19);
		getContentPane().add(lblOct);
		
		lblBin = new JLabel("");
		lblBin.setHorizontalAlignment(SwingConstants.LEFT);
		lblBin.setBounds(35, 131, 105, 19);
		getContentPane().add(lblBin);
		
		
	}
	//Aqui le transformo el valor en los otros tipos númericos
	
	public static void transformarNumero() {
		lblHex.setText(Integer.toHexString(Integer.parseInt(lblResultado.getText())));
		lblDec.setText(lblResultado.getText());
		lblOct.setText(Integer.toOctalString(Integer.parseInt(lblResultado.getText())));
		lblBin.setText(convertirBinario(Integer.parseInt(lblResultado.getText())));
	}
	public static String convertirBinario(long decimal) {
		if (decimal <= 0) {
			return "0";
		}
		StringBuilder binario = new StringBuilder();
		while (decimal > 0) {
			short residuo = (short) (decimal % 2);
			decimal = decimal / 2;
			// Insertar el dígito al inicio de la cadena
			binario.insert(0, String.valueOf(residuo));
		}
		return binario.toString();
	}
	public static void vaciarLbl() {
		lblHex.setText("");
		lblDec.setText("");
		lblOct.setText("");
		lblBin.setText("");
	}

}

