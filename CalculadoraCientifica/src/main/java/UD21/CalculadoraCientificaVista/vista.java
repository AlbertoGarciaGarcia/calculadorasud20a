package UD21.CalculadoraCientificaVista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class vista extends JFrame {

	private JPanel contentPane;
	
	public static JLabel numero = new JLabel(" ");
	public static  JLabel resultado  = new JLabel(" ");
	public static double operacion;
	public static double n1;
	public static double n2;
	public static String ultimoCalculo;
	public static JTextArea historial;

	public vista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 559, 543);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		numero = new JLabel();
		numero.setBounds(10, 11, 94, 29);
		contentPane.add(numero);

		 historial = new JTextArea();
		historial.setBounds(388, 71, 145, 422);
		contentPane.add(historial);
		resultado.setBounds(20, 51, 46, 14);
		contentPane.add(resultado);
		JLabel lblNewLabel = new JLabel("Historial");
		lblNewLabel.setBounds(450, 11, 83, 14);
		contentPane.add(lblNewLabel);
	
		JButton btnNewButton = new JButton("1");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"1");
			}
		});
		btnNewButton.setBounds(143, 412, 56, 41);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("2");
		btnNewButton_1.setBounds(199, 412, 56, 41);
		contentPane.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"2");
			}
		});
		
		JButton btnNewButton_1_1 = new JButton("3");
		btnNewButton_1_1.setBounds(252, 412, 56, 41);
		contentPane.add(btnNewButton_1_1);
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"3");
			}
		});
		
		JButton btnNewButton_1_2 = new JButton("+");
		btnNewButton_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ultimoCalculo = "+";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + "+");
				numero.setText(" ");
			}
		});
		btnNewButton_1_2.setBounds(305, 412, 56, 41);
		contentPane.add(btnNewButton_1_2);
		
		JButton btnNewButton_2 = new JButton("4");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"4");
			}
		});
		btnNewButton_2.setBounds(143, 370, 56, 41);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("7");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"7");
			}
		});
		btnNewButton_3.setBounds(143, 331, 56, 41);
		contentPane.add(btnNewButton_3);

		
		
		
		JButton btnNewButton_5 = new JButton("π");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numeroPi(n1,n2,operacion,numero,resultado,historial);
			}
		});
		btnNewButton_5.setBounds(87, 218, 112, 41);
		contentPane.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("5");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"5");
			}
		});
		btnNewButton_6.setBounds(199, 370, 56, 41);
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("8");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"8");
			}
		});
		btnNewButton_7.setBounds(199, 331, 56, 41);
		contentPane.add(btnNewButton_7);
		
		
	
		
		
		JButton btnNewButton_9 = new JButton("e");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(" ");
				n1 = 0;
			}
		});
		btnNewButton_9.setBounds(199, 218, 56, 41);
		contentPane.add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("6");
		btnNewButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"6");
			}
		});
		btnNewButton_10.setBounds(252, 370, 56, 41);
		contentPane.add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("9");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"9");
			}
		});
		btnNewButton_11.setBounds(252, 331, 56, 41);
		contentPane.add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("n!");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numeroFactorial(n1,n2,operacion,numero,resultado,historial);
				
				
	
			}
		});
		btnNewButton_12.setBounds(199, 298, 109, 36);
		contentPane.add(btnNewButton_12);
		
		JButton btnNewButton_13 = new JButton("C");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(" ");
				resultado.setText(" ");
				n1 = 0;
				n2 = 0;
			}
		});
		btnNewButton_13.setBounds(252, 218, 56, 41);
		contentPane.add(btnNewButton_13);
		
		JButton btnNewButton_14 = new JButton("-");
		btnNewButton_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				ultimoCalculo = "-";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + "-");
				numero.setText(" ");
			}
		});
		btnNewButton_14.setBounds(305, 370, 56, 41);
		contentPane.add(btnNewButton_14);
		
		JButton btnNewButton_15 = new JButton("*");
		btnNewButton_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ultimoCalculo = "*";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + "*");
				numero.setText(" ");
			}
			
		});
		btnNewButton_15.setBounds(305, 331, 56, 41);
		contentPane.add(btnNewButton_15);
		
		JButton btnNewButton_16 = new JButton("/");
		btnNewButton_16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ultimoCalculo = "/";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + "/");
				numero.setText(" ");
			}
		});
		btnNewButton_16.setBounds(305, 298, 56, 41);
		contentPane.add(btnNewButton_16);
		
		JButton btnNewButton_17 = new JButton("Eliminar");
		btnNewButton_17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(""+numero.getText().substring(0, numero.getText().length() - 1));
			}
		});
		btnNewButton_17.setBounds(305, 218, 56, 41);
		contentPane.add(btnNewButton_17);
		
	
		
		JButton btnNewButton_1_3 = new JButton("0");
		btnNewButton_1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"0");
			}
		});
		btnNewButton_1_3.setBounds(199, 452, 56, 41);
		contentPane.add(btnNewButton_1_3);
		
		JButton btnNewButton_1_4 = new JButton(".");
		btnNewButton_1_4.setBounds(252, 452, 56, 41);
		contentPane.add(btnNewButton_1_4);
		btnNewButton_1_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+".");
			}
		});
		
		JButton btnNewButton_1_5 = new JButton("=");
		btnNewButton_1_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	
				if(ultimoCalculo.contentEquals("+")) {
				suma(n1,n2,operacion,numero,resultado,historial);
					
				}
				else if(ultimoCalculo.contentEquals("-")) {
					resta(n1,n2,operacion,numero,resultado,historial);
				}
				else if(ultimoCalculo.contentEquals("*")) {
					multiplicacion(n1,n2,operacion,numero,resultado,historial);
				}
				else if(ultimoCalculo.contentEquals("/")) {
					division(n1,n2,operacion,numero,resultado,historial);
				}
				else if(ultimoCalculo.contentEquals("mod")) {
					modulo(n1,n2,operacion,numero,resultado,historial);
				}
				else if(ultimoCalculo.contentEquals("exp")) {
					exp(n1,n2,operacion,numero,resultado,historial);
				}
				else if(ultimoCalculo.contentEquals("xy")) {
					xelevadoy(n1,n2,operacion,numero,resultado,historial);
				}
				
			
				
			}
		});
		btnNewButton_1_5.setBounds(305, 452, 56, 41);
		contentPane.add(btnNewButton_1_5);
		
		JButton btnNewButton_17_1 = new JButton("mod");
		btnNewButton_17_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ultimoCalculo = "mod";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + " mod ");
				numero.setText(" ");
				
			}
		});
		btnNewButton_17_1.setBounds(305, 257, 56, 41);
		contentPane.add(btnNewButton_17_1);
		
		JButton btnNewButton_17_1_1 = new JButton("exp");
		btnNewButton_17_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ultimoCalculo = "exp";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + " exp ");
				numero.setText(" ");
			}
		});
		btnNewButton_17_1_1.setBounds(252, 257, 56, 41);
		contentPane.add(btnNewButton_17_1_1);
		
		JButton btnNewButton_17_1_1_1 = new JButton("|x|");
		btnNewButton_17_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(numero.getText());
			}
		});
		btnNewButton_17_1_1_1.setBounds(199, 257, 56, 41);
		contentPane.add(btnNewButton_17_1_1_1);
		
		JButton btnNewButton_17_1_1_2 = new JButton("1/X");
		btnNewButton_17_1_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dividir1(n1,n2,operacion,numero,resultado,historial);
			}
		});
		btnNewButton_17_1_1_2.setBounds(143, 257, 56, 41);
		contentPane.add(btnNewButton_17_1_1_2);
		
		JButton btnNewButton_17_1_1_3 = new JButton("X^2");
		btnNewButton_17_1_1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				potencia(n1,n2,operacion,numero,resultado,historial);
			}
		});
		btnNewButton_17_1_1_3.setBounds(87, 257, 56, 41);
		contentPane.add(btnNewButton_17_1_1_3);
		
		JButton btnNewButton_1_3_1 = new JButton("In");
		btnNewButton_1_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				in(n1,n2,operacion,numero,resultado,historial);
			}
		});
		btnNewButton_1_3_1.setBounds(87, 452, 112, 41);
		contentPane.add(btnNewButton_1_3_1);
		
		JButton btnNewButton_1_3_2 = new JButton("log");
		btnNewButton_1_3_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log(n1,n2,operacion,numero,resultado,historial);
			}
		});
		btnNewButton_1_3_2.setBounds(87, 412, 56, 41);
		contentPane.add(btnNewButton_1_3_2);
		
		JButton btnNewButton_2_1 = new JButton("10^X");
		btnNewButton_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				elevar(n1,n2,operacion,numero,resultado,historial);
				
				
			}
		});
		btnNewButton_2_1.setBounds(87, 370, 56, 41);
		contentPane.add(btnNewButton_2_1);
		
		JButton btnNewButton_2_1_1 = new JButton("X^Y");
		btnNewButton_2_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ultimoCalculo= "xy";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + " X^Y ");
				numero.setText(" ");
				
			}
		});
		btnNewButton_2_1_1.setBounds(87, 331, 56, 41);
		contentPane.add(btnNewButton_2_1_1);
		
		JButton btnNewButton_2_1_2 = new JButton("√");
		btnNewButton_2_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				raizCuadrada(n1,n2,operacion,numero,resultado,historial);
			}
		});
		btnNewButton_2_1_2.setBounds(87, 293, 112, 41);
		contentPane.add(btnNewButton_2_1_2);
		
		
		
		setVisible(true);
		
		
		
		
	}
	
	public static double suma( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
	int parentesisAbrir = numero.getText().indexOf("(");
	int parentesisCerrar = numero.getText().lastIndexOf(")");
	
	if(parentesisAbrir == 0 && parentesisCerrar == numero.getText().lastIndexOf(")")) {
		
	}
	
	
		n2 = Double.parseDouble(numero.getText());
		operacion = n2 + n1;
		historial.setText(historial.getText()+ (n1+ " + " + n2 + " = " + operacion + "\n"));
		numero.setText(String.valueOf(operacion));
		resultado.setText(String.valueOf(operacion));
		return operacion;
		
	}
	
	public static double resta( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = Double.parseDouble(numero.getText());
		operacion = n1 - n2;
		historial.setText(historial.getText()+ (n1+ " - " + n2 + " = " + operacion + "\n"));
		numero.setText(String.valueOf(operacion));
		resultado.setText(String.valueOf(operacion));
		return operacion;
		
	}
	
	public static double multiplicacion( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = Double.parseDouble(numero.getText());
		operacion = n2 * n1;
		historial.setText(historial.getText()+ (n1+ " * " + n2 + " = " + operacion + "\n"));
		numero.setText(String.valueOf(operacion));
		resultado.setText(String.valueOf(operacion));
		return operacion;
		
	}
	public static double division( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = Double.parseDouble(numero.getText());
		operacion = n1 / n2;
		historial.setText(historial.getText()+ (n1+ " / " + n2 + " = " + operacion + "\n"));
		numero.setText(String.valueOf(operacion));
		resultado.setText(String.valueOf(operacion));
		return operacion;
		
	}
	
	

	
	public static double raizCuadrada( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =  Math.sqrt(Double.parseDouble(numero.getText()));
		historial.setText(historial.getText()+ numero.getText() + (" √ ")+ (" = ") + n2 + "\n");
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
		
	}
	
	public static double potencia( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =  ((Double.parseDouble(numero.getText()) * (Double.parseDouble(numero.getText()))));
		historial.setText(historial.getText()+numero.getText()+ (" ^2 ") + (" = ") + n2 + "\n");
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));

		return n2;
	}
	
	
	public static double dividir1( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = 1 / Double.parseDouble(numero.getText());
		historial.setText(historial.getText()+" 1 "+ (" / ") + n2 + (" = ") + operacion + "\n");
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	public static double numeroPi( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =Math.PI;
		historial.setText(historial.getText()+ n2 + (" = ") + operacion + "\n");
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	public static double numeroFactorial( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = Double.parseDouble(numero.getText());
		double factorial = 1;
		while( n2!= 0) {
			factorial = factorial*n2;
			n2--;
		}
		n2 = factorial;
		historial.setText(historial.getText()+" Factorial de "+ n2 + " es " + factorial + operacion + "\n");
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	
	public static double modulo( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =Double.parseDouble(numero.getText());
		double mod = n1 % n2;
		historial.setText(historial.getText()+ ("El mod de" + n1 + " y " + n2 + "es" + mod + "\n"));
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	public static double exp( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =Double.parseDouble(numero.getText());
		double exp = n1 * (10 * n2);
		historial.setText(historial.getText()+ ("El exp de " + n1 + " y " + n2 + "es" + exp + "\n"));
		n2 = exp;
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	public static double elevar( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =Double.parseDouble(numero.getText());
		double exp = Math.pow(10, n2);
		historial.setText(historial.getText()+ ("10^" + n2 + " = " + exp + "\n"));
		n2 = exp;
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	public static double xelevadoy( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =Double.parseDouble(numero.getText());
		double exp = Math.pow(n1, n2);
		historial.setText(historial.getText()+ (n1 +" ^ " + n2 + " = " + exp + "\n"));
		n2 = exp;
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	public static double log( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =Double.parseDouble(numero.getText());
		double exp = Math.log(n2);
		historial.setText(historial.getText()+ ("Log " + n2 + " = " + exp + "\n"));
		n2 = exp;
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	
	public static double in( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =Double.parseDouble(numero.getText());
		double exp = (-Math.log(1-n2))/n2;
		historial.setText(historial.getText()+ ("In " + n2 + " = " + exp + "\n"));
		n2 = exp;
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

