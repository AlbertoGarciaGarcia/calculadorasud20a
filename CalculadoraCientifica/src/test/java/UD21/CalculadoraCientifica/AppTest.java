package UD21.CalculadoraCientifica;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import org.junit.Test;

import UD21.CalculadoraCientificaVista.vista;
import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testsuma() {
    	vista calculadora = new vista();
		double esperado = 4;
		double delta = 1;
		vista.numero.setText("2");
		vista.operacion = 0;
		assertEquals(vista.suma(2.0, 3.0, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
		
	}
    public void testResta() {
    	vista calculadora = new vista();
		double esperado = 2;
		double delta = 1;
		vista.numero.setText("3");
		vista.operacion = 0;
		assertEquals(vista.resta(5.0, 3.0, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    
    public void testMultiplicacion() {
    	vista calculadora = new vista();
		double esperado = 15;
		double delta = 1;
		vista.numero.setText("3");
		vista.operacion = 0;
		assertEquals(vista.multiplicacion(5.0, 3.0, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    public void testDivision() {
    	vista calculadora = new vista();
		double esperado = 2;
		double delta = 1;
		vista.numero.setText("3");
		vista.operacion = 0;
		assertEquals(vista.division(6.0, 3.0, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}

    public void testraizCuadrada() {
    	vista calculadora = new vista();
		double esperado = 3;
		double delta = 1;
		vista.numero.setText("9");
		vista.operacion = 0;
		assertEquals(vista.raizCuadrada(1,1, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    
    public void testPotencia() {
    	vista calculadora = new vista();
		double esperado = 4;
		double delta = 1;
		vista.numero.setText("2");
		vista.operacion = 0;
		assertEquals(vista.potencia(1,1, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    public void testdividir1() {
    	vista calculadora = new vista();
		double esperado = 0.1;
		double delta = 1;
		vista.numero.setText("10");
		vista.operacion = 0;
		assertEquals(vista.dividir1(1,1, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
 
    public void testpi() {
    	vista calculadora = new vista();
		double esperado = 3.14;
		double delta = 1;
		vista.numero.setText("0");
		vista.operacion = 0;
		assertEquals(vista.numeroPi(1,1, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    public void testFactorial() {
    	vista calculadora = new vista();
		double esperado = 6;
		double delta = 1;
		vista.numero.setText("3");
		vista.operacion = 0;
		assertEquals(vista.numeroFactorial(3,3, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    public void testmodulo() {
    	vista calculadora = new vista();
		double esperado = 2;
		double delta = 1;
		vista.numero.setText("2");
		vista.operacion = 0;
		assertEquals(vista.modulo(6,4, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    public void testexp() {
    	vista calculadora = new vista();
		double esperado = 100;
		double delta = 1;
		vista.numero.setText("10");
		vista.operacion = 0;
		assertEquals(vista.exp(1,2, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    public void testelevar() {
    	vista calculadora = new vista();
		double esperado = 10;
		double delta = 1;
		vista.numero.setText("1");
		vista.operacion = 0;
		assertEquals(vista.elevar(1,1, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    public void testxelevary() {
    	vista calculadora = new vista();
		double esperado = 4;
		double delta = 1;
		vista.numero.setText("2");
		vista.operacion = 0;
		assertEquals(vista.xelevadoy(2,2, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    public void testlog() {
    	vista calculadora = new vista();
		double esperado = 2.3;
		double delta = 1;
		vista.numero.setText("10");
		vista.operacion = 0;
		assertEquals(vista.log(1,1, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    public void testin() {
    	vista calculadora = new vista();
		double esperado = 1.3;
		double delta = 1;
		vista.numero.setText("0.5");
		vista.operacion = 0;
		assertEquals(vista.in(1,1, vista.operacion,vista.numero, vista.resultado, vista.historial),esperado,delta);
  	}
    

}
